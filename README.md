### The Vintage Invite Company Website

This repository holds the latest code for the static parts of [http://thevintageinvite.com/](http://thevintageinvite.com/).
The discussion of how it was built is in associated the pages blog.

Uses nanoc, compass and Zurb's Foundation.

### LICENSING

Everything in `content/assets/images/` (not including `foundation` or anything else that isn't ours...), `content/blog/posts` is (c) The Vintage Invite Company.

Vendor libraries (compass/zurb have their associated licenses).

All the layout and styling code is MIT licensed.

Hi Sarah,

Apologies for the delay in sorting this out – we clearly chose the worst time of the year to present a challenging design to our printer, and we had to wait until the senior technician could have a proper chat with us to explain our options.

The problem is this: our geometric design is both complicated and very precise – the triangles of colour are all touching each other, and so to reproduce accurately requires precision printing. This is fine with one colour, slightly harder with two, and harder still with three colours. This is because the letterpress process smashes an inked plate at high pressure into the card for each colour; each time this happens the paper gets slightly compressed and distorted, and the cumulative effect of this over three colours can mean that the card has changed size enough to make a noticeable difference to the precision. One of the beautiful aspects of letterpress printing is the minor imperfections that give the resulting piece character, but with our design, this could create tiny gaps between the triangles, which is what the printer was warning us about.

There are a few ways we can move forward with this (outside of overhauling the design itself): first is that we can change the card we were planning to use for the cover; we were looking at using a cotton paper (similar to the sample invitation we sent to you, only thinner so we can fold it!), but this requires the most pressure to make sure the ink is absorbed, and is also a bit squishier, so will react the most. Instead we could use a denser card (something more like Oli and Siobhan’s cover) – this will not have as deep an indentation, but is more likely to behave itself!

There’s still a small risk of tiny alignment issues with the denser card, so if you do choose to go ahead it is something to bear in mind.

The other alternative is to go completely digital. This will mean a very faithful reproduction of your design, but no indentation. It is cheaper, so of course we would either use the deposit you paid to cover more of the extra stationery costs, or refund you the difference in the meantime.

So there’s a decision to be made: go for letterpress and accept the (small) risk, or go for digital. We’re sending a digital sample of one of our other designs to you today, so you may want to wait until you receive this before you make your decision. I am confident that the letterpress printers will do a great job, but it wouldn’t be fair to go ahead with it without letting you know the risks!

Let us know what you think – I’m happy to have a chat with you about it all if you have more questions (or if I didn’t explain things clearly enough!).

Thanks

Colin

PS Apologies for the long-winded e-mail, I couldn’t get it all out any more succinctly!
